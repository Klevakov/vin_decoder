import random

import factory
from faker.providers import BaseProvider

from vin_decoder.models import Car, Weight


class CustomProvider(BaseProvider):
    @staticmethod
    def vin():
        return random.choice(
            ['1P3EW65F4VV300946', 'SCBBR9ZA1AC063223', 'JN8DR07XX1W514175']
        )

    @staticmethod
    def year():
        return random.choice([2010, 2015, 2020])

    @staticmethod
    def make():
        return random.choice(['Nissan', 'Toyota', 'BMW'])

    @staticmethod
    def model():
        return random.choice(['Camry', 'Mark II', 'X6', 'M5', 'Qashqai'])

    @staticmethod
    def car_type():
        return random.choice(['Sedan', 'Coupe', 'Hatchback', 'SportCar'])

    @staticmethod
    def color():
        return random.choice(['yellow', 'red', 'green', 'black'])

    @staticmethod
    def dimensions(number):
        """
        :param number:
        :return: a dict that contains random data
        """
        return {
            'email': 'example{0}@foo.com'.format(number),
            'username': 'username{0}'.format(number),
        }

    @staticmethod
    def weight_type():
        return random.choice(['Curb Weight', 'Standard Towing', 'Standard GVWR'])

    @staticmethod
    def unit():
        return random.choice(['yellow', 'red', 'green', 'black'])

    @staticmethod
    def value():
        return random.choice([3985, 5000, 5050, 5568, 4568])


factory.Faker.add_provider(CustomProvider)


class CarFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Car

    vin = factory.Faker('vin')
    year = factory.Faker('year')
    make = factory.Faker('make')
    model = factory.Faker('model')
    car_type = factory.Faker('car_type')
    color = factory.Faker('color')
    dimensions = factory.Sequence('dimensions')


class WeightFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Weight

    id = factory.Sequence(lambda n: n)
    type = factory.Faker('weight_type')
    unit = 'lbs'
    value = factory.Faker('value')
    car = factory.SubFactory(CarFactory)

    @factory.post_generation
    def car(self, create, extracted, **kwargs):
        if not create or not extracted:
            # Simple build, or nothing to add, do nothing.
            return

        # Add the iterable of groups using bulk addition
        self.car.add(*extracted)

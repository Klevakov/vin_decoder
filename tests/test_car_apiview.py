from unittest.mock import patch

from rest_framework.test import APIClient, APITestCase

from tests.data_factories import CarFactory, WeightFactory
from tests.test_data.data_loader import BaseYamlTestDataLoader
from vin_decoder.connector import FirstSupplier
from vin_decoder.models import Car


class TestProductViewSet(BaseYamlTestDataLoader, APITestCase):
    BASE_URL = '/api/v1/vin/'
    test_data_files = {'all': 'tests/test_data/vin_test_data.yml'}
    test_vin_number = 'SCBBR9ZA1AC063223'

    def setUp(self) -> None:
        self.api_client = APIClient()
        self.test_data = self.load_test_data()['all']

    @patch('vin_decoder.connector.FirstSupplier.get_vin_info')
    def test_get_detail_info_about_car_by_supplier(self, mock_get_vin_info):
        """
        The test checks the API to return detailed vehicle information by VIN
        received from the supplier.

         Expected Result:
            - response status 200
            - the returned value is the same as expected.
        """

        mock_get_vin_info.return_value = self.test_data['supplier_return_value']

        response = self.api_client.get(
            self.BASE_URL + f'{self.test_vin_number}/',
            content_type='application/json',
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(mock_get_vin_info.call_count, 1)
        self.assertEqual(response.data, self.test_data['get_vin_expected_data'])

    @patch('vin_decoder.connector.FirstSupplier.get_vin_info')
    def test_get_detail_info_about_car_by_db(self, mock_get_vin_info):
        """
        The test checks the API to return detailed vehicle information by VIN number
        retrieved from a database.

         Expected Result:
            - response status 200
            - the returned value is the same as expected.
        """

        car = CarFactory(**self.test_data['faker_car']['car_info'])
        WeightFactory(car_id=car.vin, **self.test_data['faker_car']['car_weight'])

        response = self.api_client.get(
            self.BASE_URL + f'{self.test_vin_number}/',
            content_type='application/json',
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(mock_get_vin_info.call_count, 0)
        self.assertEqual(response.data, self.test_data['get_vin_expected_data'])

    @patch.object(FirstSupplier, 'get_vin_info')
    def test_bad_request(self, mock_method):
        """
        The test checks the API to return the response status 404
        in case of passing an incorrect VIN.

         Expected Result:
            - response status 404
            - information about the car is not saved to database.
        """

        bad_vin_number = 'SCBBR9ZA1AC063224'
        mock_method.return_value = self.test_data['vin_not_found_supplier_number']

        response = self.api_client.get(
            self.BASE_URL + f'{bad_vin_number}/', content_type='application/json'
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(mock_method.call_count, 1)
        with self.assertRaises(Car.DoesNotExist):
            Car.objects.get(vin=bad_vin_number)

    def test_bad_supplier_response_status(self):
        """
        API response test to return a 500 response response
        if the provider's service returned a response status other than 200.

         Expected Result:
            - response status 500
            - information about the car is not saved to database.
        """

        with patch('requests.get') as mock_request:
            mock_request.return_value.status_code = 500

            response = self.api_client.get(
                self.BASE_URL + f'{self.test_vin_number}/',
                content_type='application/json',
            )
        self.assertEqual(response.status_code, 500)
        with self.assertRaises(Car.DoesNotExist):
            Car.objects.get(vin=self.test_vin_number)

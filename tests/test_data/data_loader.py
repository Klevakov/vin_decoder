import os

import yaml
from django.conf import settings


class BaseYamlTestDataLoader:
    """
    Base class for loading test data from yml files.
    """

    # File names in the format:
    # {'name': '/file_path.yml'}
    test_data_files = None

    @classmethod
    def load_test_data(cls):
        """
        Loads test data from yml files.

        :return: test data
        """

        result = {}
        for name, file_path in cls.test_data_files.items():
            test_data_file = os.path.join(settings.BASE_DIR, file_path)
            with open(test_data_file, 'r') as stub_file:
                result.update({name: yaml.load(stub_file, Loader=yaml.Loader)})
        return result

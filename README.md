# VIN decoder

Microservice for receiving and storing information about a car by VIN code.

Technology stack:

    - Django
    - DjangoRestFramework
    - Docker
    - SWAGGER

## Deployment on the developer's machine

* Cloning [VIN_decoder](https://gitlab.com/Klevakov/vin_decoder).
  ```bash
  git clone git@gitlab.com:Klevakov/vin_decoder.git
  ```
* Go to the vin_decoder directory and collect the project image:

  ```bash
  docker build -t vin_decoder -f docker/Dockerfile .
  ```

* Go to the docker directory and run the created image along with the database server image:

  ```bash
  docker-compose up
  ```


## After deploying the application on the developer's machine, the following APIs will be available to you:
* The [swagger-ui](http://0.0.0.0:8000/swagger/) API specification view

* [ReDoc](http://0.0.0.0:8000/redoc/) representation of the API specification

* [YAML](http://0.0.0.0:8000/swagger.yaml) representation of the API specification

* The only API access point for obtaining information about a car by VIN number
  http://0.0.0.0:8000/vin/<your_vin_code>/

## Switching to another VIN decoding provider
To switch to another service provider, you need to:
1. Create a Supplier class in the vin_decoder.connector module, inheriting it from the BaseSupplier class
2. Change the URI address of the service provider in the BASE_SUPPLIER_URL environment variable
3. Write the name of the class from point 1 in the environment variable SUPPLIER


## DataBase scheme
![Image alt](logo/db_scheme.png)


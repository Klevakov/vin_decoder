from abc import ABC, abstractmethod

import requests

from conf.settings import BASE_SUPPLIER_URL
from vin_decoder.exceptions import VinNotFoundException, SupplierNotAvailableException


class BaseSupplier(ABC):
    def __init__(self, vin):
        self.vin = vin

    @abstractmethod
    def get_vin_info(self):
        """
        Returns detailed information about the car by decoded VIN code
        received by API from the supplier.

        :param: vin: '1P3EW65F4VV300946'
        :return: detailed information about the car
        """
        pass

    @abstractmethod
    def get_data(self):
        """
        The method implements the transformation of information
        received from the supplier via the API into the required format.

        :return: data in required format: {
            'vin': 'JN8DR07XX1W514175',
            'year': 2001,
            'make': 'Nissan',
            'model': 'Pathfinder',
            'car_type': 'SPORT UTILITY 4-DR',
            'color': 'yellow',
            'dimensions': {
                'Cargo Length': '32.70',
            },
            'weight': [
                {'type': 'Curb Weight', 'unit': 'lbs', 'value': 3985},
            ],
        }
        """
        pass


class FirstSupplier(BaseSupplier):
    def get_vin_info(self):
        """
        Returns detailed information about the vehicle received
        in the supplier format.

        :return: {
            'decode': {
                'status': 'SUCCESS',
                'Valid': 'True',
                'vehicle': [
                    {
                        'body': 'CONVERTIBLE 2-DR',
                        'driveline': 'RWD',
                        'engine': 'Gas V6',
                        'Equip': [
                            {'name': 'Engine Type', 'unit': "", 'value': 'Gas V6'},
                        ],
                        'dimensions': {
                            'Wheelbase': '112.50',
                        },
                        'weight': {
                            'type': 'Curb Weight', 'unit': 'lbs', 'value': 5568
                        },
                        'id': '110153',
                        'make': 'Plymouth',
                        'model': 'Prowler',
                        'color': 'Yellow',
                        'trim': 'Base',
                        'year': '1997',
                    },
                ],
                'version': 'DECODE',
                'VIN': '1P3EW65F4VV300946',
            }
        }
        """

        response = requests.get(
            BASE_SUPPLIER_URL.format(vin=self.vin),
            headers={"content_type": "application/json"},
        )
        if response.status_code != 200:
            raise SupplierNotAvailableException(self.vin)
        return response.json()

    def get_data(self):
        data = self.get_vin_info().get('decode', {})
        if data['status'] != 'SUCCESS':
            raise VinNotFoundException(data['VIN'])
        vehicle_data = data['vehicle'][0]
        return {
            'vin': data['VIN'],
            'year': vehicle_data['year'],
            'make': vehicle_data['make'],
            'model': vehicle_data['model'],
            'car_type': vehicle_data['body'],
            'color': vehicle_data['color'],
            'dimensions': vehicle_data['dimensions'],
            'weight': [vehicle_data['weight']]
            if isinstance(vehicle_data['weight'], dict)
            else vehicle_data['weight'],
        }

class SupplierNotAvailableException(Exception):
    def __init__(self, vin):
        self.vin = vin
        self.message = f"VIN '{vin}' not found in supplier's system."


class VinNotFoundException(Exception):
    def __init__(self, vin):
        self.vin = vin
        self.message = f"VIN '{vin}' not found in supplier's system."

from importlib import import_module

from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from conf.settings import SUPPLIER
from vin_decoder.exceptions import VinNotFoundException, SupplierNotAvailableException
from vin_decoder.models import Car
from vin_decoder.serializaers import CarSerializer


cls_supplier = getattr(import_module('vin_decoder.connector'), SUPPLIER)


class CarAPIView(APIView):
    serializer = CarSerializer

    def get(self, request, *args, **kwargs):
        vin = kwargs.get('vin')
        try:
            car_info = Car.objects.get(vin=vin)
        except ObjectDoesNotExist:

            try:
                data = cls_supplier(vin).get_data()
            except VinNotFoundException:
                return Response(status=status.HTTP_404_NOT_FOUND)
            except SupplierNotAvailableException:
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            serializer = self.serializer(data=data)
            if serializer.is_valid(raise_exception=True):
                car_info = serializer.save()

        return Response(CarSerializer(car_info).data)

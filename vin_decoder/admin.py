from django.contrib import admin

from vin_decoder.models import Car, Weight


@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
    list_display = [
        'vin', 'year', 'make', 'model', 'car_type', 'color', 'dimensions'
    ]
    ordering = ['model']
    list_per_page = 5


@admin.register(Weight)
class WeightAdmin(admin.ModelAdmin):
    list_display = ['id', 'type', 'unit', 'value']
    ordering = ['type']
    list_per_page = 10

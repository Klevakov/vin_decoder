from rest_framework import serializers

from vin_decoder.models import Car, Weight


class WeightSerializer(serializers.ModelSerializer):
    class Meta:
        model = Weight
        fields = ['type', 'unit', 'value']


class CarSerializer(serializers.ModelSerializer):
    weight = WeightSerializer(many=True)

    class Meta:
        model = Car
        fields = [
            'vin',
            'year',
            'make',
            'model',
            'car_type',
            'color',
            'dimensions',
            'weight',
        ]

    def create(self, validated_data):
        weights_data = validated_data.pop('weight')
        car = Car.objects.create(**validated_data)
        for weight_data in weights_data:
            Weight.objects.create(car=car, **weight_data)
        return car

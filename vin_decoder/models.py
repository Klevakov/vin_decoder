from django.db import models


class Car(models.Model):
    vin = models.CharField(
        max_length=17, verbose_name='VIN number', primary_key=True
    )
    year = models.IntegerField(verbose_name='Vehicle year', null=True, blank=True)
    make = models.CharField(
        max_length=100, verbose_name="Manufacturer's name", null=True, blank=True
    )
    model = models.CharField(
        max_length=50, verbose_name='Car model', null=True, blank=True
    )
    car_type = models.CharField(
        max_length=50, verbose_name='Car body type', null=True, blank=True
    )
    color = models.CharField(
        max_length=50, verbose_name='Car color', null=True, blank=True
    )
    dimensions = models.JSONField(
        default=dict, verbose_name='Dimensions', null=True, blank=True
    )

    def __str__(self):
        return f'{self.make} {self.model} {self.year} year of issue'

    @property
    def weight(self):
        """
        Returns a list of dictionaries with car mass data.

        :return: [
            {
                "type": "Curb Weight",
                "unit": "lbs",
                "value": 3985
            },
        ]
        """

        return [
            {
                'type': weight['type'],
                'unit': weight['unit'],
                'value': weight['value'],
            }
            for weight in self.weights.values()
        ]


class Weight(models.Model):
    car = models.ForeignKey(
        Car,
        on_delete=models.CASCADE,
        related_name='weights',
        verbose_name='Vehicle weight',
    )
    type = models.CharField(
        max_length=50, verbose_name='Weight type', null=True, blank=True
    )
    unit = models.CharField(
        max_length=50, verbose_name='Weight unit', null=True, blank=True
    )
    value = models.IntegerField(verbose_name='Weight value', null=True, blank=True)
